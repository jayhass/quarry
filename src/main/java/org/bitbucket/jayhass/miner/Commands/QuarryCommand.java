package org.bitbucket.jayhass.miner.Commands;

import org.apache.commons.lang.ArrayUtils;
import org.bitbucket.jayhass.miner.Events;
import org.bitbucket.jayhass.miner.Miner;
import org.bitbucket.jayhass.miner.PlayerHolder;
import org.bitbucket.jayhass.miner.Quarry;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;


import java.util.Arrays;
import java.util.HashMap;
import java.util.UUID;


public class QuarryCommand implements CommandExecutor {

    //Locations of all the quarry blocks to be used in StoneBreak event
    public static Location[] quarryBlockLocations = new Location[]{};

    //Locations of redstone wires to be used in redstone break event. i.e. if a redstone wire that forms
    //a quarry is broken, the quarry will be broken too.
    public static Location[][] quarryRedstoneLocations = new Location[][]{};

    //Hashmap is to provide an easy way to get the location of the quarry block to break if wire that makes
    //up said quarry is broken.
    public static HashMap<Location[], Location> redstoneBorderToQuarryBlock = new HashMap<>();


    //Hashmap used to get the quarry object being referenced on a stone break event
    public static HashMap<Location, Quarry> locationToQuarry = new HashMap<>();


    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (sender instanceof Player) {

            Player p = (Player) sender;
            UUID id = p.getUniqueId();
            PlayerHolder h = Events.players.get(id);

            if (!h.isPlacingQuarry) {

                h.isPlacingQuarry = true;
                p.sendMessage(ChatColor.DARK_GREEN + "You are now placing a quarry");

            } else {

                h.isPlacingQuarry = false;

                if (isClosed(h.redstoneLocations, p)) {

                    p.sendMessage(ChatColor.GOLD + "The quarry is closed");
                    Quarry quarry = new Quarry(h.redstoneLocations, p);
                    Location stoneBlock = quarry.spawnQuarryBlock(h.redstoneLocations, p);
                    locationToQuarry.put(stoneBlock, quarry);
                    quarryRedstoneLocations = (Location[][]) ArrayUtils.add(quarryRedstoneLocations, h.redstoneLocations);

                } else {

                    for (Location redstoneLocation : h.redstoneLocations) {

                        Block redstone = redstoneLocation.getBlock();
                        redstone.breakNaturally();
                    }
                }

                h.redstoneLocations = Arrays.copyOf(h.redstoneLocations, 0);
            }
        }
        return true;
    }



    //Method checks that the redstone forms a complete path with no branches that is not too long.
    private boolean isClosed(Location[] locations, Player p) {

        if (locations.length == 0) {

            p.sendMessage(ChatColor.RED + "You haven't placed any redstone");
            return false;
        }

        Location firstLoc = locations[0].clone();
        Location pathLocation = locations[0].clone();
        Vector lastPath = new Vector(0, 0, 0);
        int pathLength = 0;

        //Possible directions of a redstone wire
        Vector posX = new Vector(1, 0, 0);
        Vector posXposY = new Vector(1, 1, 0);
        Vector posXnegY = new Vector(1, -1, 0);
        Vector negX = new Vector(-1, 0, 0);
        Vector negXposY = new Vector(-1, 1, 0);
        Vector negXnegY = new Vector(-1, -1, 0);
        Vector posZ = new Vector(0, 0, 1);
        Vector posZposY = new Vector(0, 1, 1);
        Vector posZnegY = new Vector(0, -1, 1);
        Vector negZ = new Vector(0, 0, -1);
        Vector negZposY = new Vector(0, 1, -1);
        Vector negZnegY = new Vector(0, -1, -1);

        Vector[] directions = new Vector[] {posX, posXposY, posXnegY, negX, negXposY, negXnegY, posZ, posZposY, posZnegY, negZ, negZposY, negZnegY};

        //firstLoc and pathLocation are initialized to the same location, so must take the first step before assessing
        //whether the two are equal.
        do {

            Vector[] possiblePaths = new Vector[]{};

            //Find the possible paths first.
            for (Vector direction : directions) {

                pathLocation.add(direction);

                Block b = pathLocation.getBlock();
                Material m = b.getType();

                if (m == Material.REDSTONE_WIRE) {

                    possiblePaths = (Vector[]) ArrayUtils.add(possiblePaths, direction);
                }

                pathLocation.subtract(direction);
            }

            //Update pathLocation second.
            if (possiblePaths.length > 2) {

                p.sendMessage(ChatColor.RED + "Please make sure there is only 1 closed path, i.e. no dead ends or branches");
                return false;

            } else if (possiblePaths.length < 2) {

                p.sendMessage(ChatColor.RED + "No complete paths found");
                return false;

            } else {
                //2 possible paths, like expected

                //Last path is multiplied by -1 to ensure that the function does not traverse backward.
                lastPath.multiply(-1);


                //One of the two paths MUST be equal to the previously traveled path, just need to check which one.
                if (!possiblePaths[0].equals(lastPath)) {

                    pathLocation.add(possiblePaths[0]);
                    lastPath = possiblePaths[0].clone();

                } else if (!possiblePaths[1].equals(lastPath)) {

                    pathLocation.add(possiblePaths[1]);
                    lastPath = possiblePaths[1].clone();

                } else {

                    p.sendMessage("Not sure how this would happen, thought I should add a check just in case");
                }

                locations = (Location[]) ArrayUtils.removeElement(locations, pathLocation);

            }

            if (pathLength > 200) {

                p.sendMessage(ChatColor.RED + "Quarry is too large.");
                return false;
            }

            pathLength++;

        } while (!pathLocation.equals(firstLoc));

        if (locations.length > 0) {

            p.sendMessage(ChatColor.RED + "Make sure it's one closed path");
            return false;
        }

        return true;
    }
}
