package org.bitbucket.jayhass.miner;

import org.apache.commons.lang.ArrayUtils;
import org.bitbucket.jayhass.miner.Commands.QuarryCommand;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import java.util.*;

public class Quarry {

    private int lastMinedY = 255;
    private int maxX;
    private int minX;

    private HashMap<Integer, int[]> xToZBounds = new HashMap<>();

    public Quarry(Location[] locations, Player p) {

        getBounds(locations, p);
    }

    private void getBounds(Location[] locations, Player p) {

        int maxXVal = (int) locations[0].getX();
        int minXVal = (int) locations[0].getX();

        int[][] xAndZCoords = new int[][]{};

        for (Location location : locations) {

            if (location.getX() > maxXVal) {

                maxXVal = (int) location.getX();
            }

            if (location.getX() < minXVal) {

                minXVal = (int) location.getX();
            }

            int[] locationXAndZ = new int[]{(int) location.getX(), (int) location.getZ()};

            xAndZCoords = (int[][]) ArrayUtils.add(xAndZCoords, locationXAndZ);
        }

        //Add and subtract one because want to get blocks inside of the quarry, not the redstone itself
        maxXVal -= 1;
        minXVal += 1;

        maxX = maxXVal;
        minX = minXVal;

        int currentX = maxXVal;

        while (currentX >= minXVal) {

            int[] zRedstoneBounds = new int[]{};

            for (Location redstoneLocation : locations) {

                if (redstoneLocation.getX() == currentX) {

                    zRedstoneBounds = ArrayUtils.add(zRedstoneBounds, (int) redstoneLocation.getZ());
                }
            }

            Arrays.sort(zRedstoneBounds);

            int zMin = zRedstoneBounds[0];
            int zMax = zRedstoneBounds[zRedstoneBounds.length - 1];

            int currentZ = zMin - 1;

            //-1 = Outside
            //1 = Inside
            int state = -1;

            int[] zBounds = new int[]{};

            while (currentZ <= zMax) {

                if (ArrayUtils.contains(zRedstoneBounds, currentZ)) {

                    int tempZ = currentZ;

                    while (ArrayUtils.contains(zRedstoneBounds, tempZ + 1)) {

                        tempZ++;
                    }

                    if (tempZ != currentZ) {

                        //currentZ is lower bound, tempZ is upper bound
                        //Direction of 1 = right
                        //Direction of -1 = left
                        int directionLower;
                        int directionUpper;

                        int[] vertexUpperRightXAndZ = new int[]{currentX + 1, tempZ};
                        int[] vertexUpperLeftXAndZ = new int[]{currentX - 1, tempZ};

                        int[] vertexLowerRightXAndZ = new int[]{currentX + 1, currentZ};
                        int[] vertexLowerLeftXAndZ = new int[]{currentX - 1, currentZ};

                        if (Miner.ArrayOfIntArraysContains(xAndZCoords, vertexUpperLeftXAndZ)) {

                            directionUpper = -1;
                        } else if (Miner.ArrayOfIntArraysContains(xAndZCoords, vertexUpperRightXAndZ)) {

                            directionUpper = 1;
                        } else {

                            directionUpper = 0;
                        }

                        if (Miner.ArrayOfIntArraysContains(xAndZCoords, vertexLowerLeftXAndZ)) {

                            directionLower = -1;
                        } else if (Miner.ArrayOfIntArraysContains(xAndZCoords, vertexLowerRightXAndZ)) {

                            directionLower = 1;
                        } else {

                            directionLower = 0;
                        }

                        if (directionUpper != directionLower) {
                            
                            if (state == -1) {
                                
                                zBounds = ArrayUtils.add(zBounds, tempZ);
                                
                            } else if (state == 1) {
                                
                                zBounds = ArrayUtils.add(zBounds, currentZ);
                            }

                            state *= -1;

                        } else {

                            if (state == 1) {

                                zBounds = ArrayUtils.add(zBounds, currentZ);
                                zBounds = ArrayUtils.add(zBounds, tempZ);
                            }
                        }

                        currentZ = tempZ;

                    } else {

                        zBounds = ArrayUtils.add(zBounds, currentZ);
                        state *= -1;
                    }
                }
                currentZ++;
            }

            xToZBounds.put(currentX, zBounds);
            currentX -= 1;
        }
    }

    Block getNextBlock(Player p) {

        int currentY = lastMinedY;

        while (currentY != 0) {

            int currentX = maxX;

            while (currentX >= minX) {

                int[] zBounds = xToZBounds.get(currentX);

                int minZBound = zBounds[0];
                int maxZBound = zBounds[zBounds.length - 1];

                minZBound += 1;
                maxZBound -= 1;

                int currentZ = minZBound;

                int i = 1;

                while (currentZ <= maxZBound) {

                    int nextUpperBound;
                    int nextLowerBound;

                    //Check to see if it's the last bound
                    if (i + 2 > zBounds.length) {

                        nextUpperBound = zBounds[i];
                        nextLowerBound = zBounds[i];

                    } else {

                         nextUpperBound = zBounds[i];
                         nextLowerBound = zBounds[i + 1];
                    }

                    while (currentZ < nextUpperBound) {

                        Location currentLocation = new Location(p.getWorld(), currentX, currentY, currentZ);
                        Block currentBlock = currentLocation.getBlock();

                        if (currentBlock.getType() != Material.AIR) {

                            if (currentBlock.getType() == Material.BEDROCK) {

                                p.sendMessage(ChatColor.AQUA + "You have hit bedrock");
                                return null;
                            }

                            lastMinedY = currentY;
                            return currentBlock;
                        }
                        currentZ += 1;
                    }

                    currentZ = nextLowerBound + 1;

                    i += 2;
                }

                currentX -= 1;
            }
            currentY -= 1;
        }

        p.sendMessage(ChatColor.RED + "No Block was found");
        return null;
    }


    public Location spawnQuarryBlock(Location[] locations, Player p) {

        UUID id = p.getUniqueId();
        PlayerHolder h = Events.players.get(id);

        Location blockLocation = locations[0].clone();
        blockLocation.add(0, 1, 0);
        //Quarry Block will be spawned one y above the first-placed redstone wire
        Block b = blockLocation.getBlock();
        Material m = b.getType();

        if (m != Material.AIR) {

            p.sendMessage("Make sure the block above the first redstone wire is air");

        } else {

            b.setType(Material.STONE);
            QuarryCommand.quarryBlockLocations = (Location[]) ArrayUtils.add(QuarryCommand.quarryBlockLocations, blockLocation);
        }

        QuarryCommand.redstoneBorderToQuarryBlock.put(h.redstoneLocations, blockLocation);

        return blockLocation;
    }



















































































}
