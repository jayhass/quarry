package org.bitbucket.jayhass.miner;


import org.apache.commons.lang.ArrayUtils;

import org.bitbucket.jayhass.miner.Commands.QuarryCommand;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerBedEnterEvent;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class Events implements Listener {

    public static HashMap<UUID, PlayerHolder> players = new HashMap<>();

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {

        Player p = e.getPlayer();
        UUID id = p.getUniqueId();
        PlayerHolder h = new PlayerHolder(p, id);
        players.put(id, h);

    }


    @EventHandler
    public void onRedstonePlace(BlockPlaceEvent e) {

        Player p = e.getPlayer();
        UUID id = p.getUniqueId();
        PlayerHolder h = players.get(id);

        Block b = e.getBlockPlaced();
        Material m = b.getType();

        if (m == Material.REDSTONE_WIRE) {

            if (h.isPlacingQuarry) {

                int l = h.redstoneLocations.length;
                Location loc = b.getLocation();

                h.redstoneLocations = Arrays.copyOf(h.redstoneLocations, l + 1);
                h.redstoneLocations[l] = loc;
            }
        }
    }

    @EventHandler
    public void onRedstoneBreak(BlockBreakEvent e) {

        Player p = e.getPlayer();
        Block b = e.getBlock();
        Material m = b.getType();
        Location loc = b.getLocation();

        if (m == Material.REDSTONE_WIRE) {

            //Check every player to see if the redstone is included in their quarry redstone set
            //Remove it if it does
            for (Map.Entry entry : players.entrySet()) {

                PlayerHolder h = (PlayerHolder) entry.getValue();

                if (h.isPlacingQuarry) {

                    if (ArrayUtils.contains(h.redstoneLocations, loc)) {

                        h.redstoneLocations = (Location[]) ArrayUtils.removeElement(h.redstoneLocations, loc);
                    }
                }
            }

            //Check if the redstone wire is contained in a quarry border
            //Check if the quarryBlockLocations array contains the hashmap value of l (should ALWAYS be true)
            //Set the quarry block to air then remove it from the block locations so it functions as a normal block
            for (Location[] locations : QuarryCommand.quarryRedstoneLocations) {

                if (ArrayUtils.contains(locations, loc)) {

                    Location quarryBlockLocation = QuarryCommand.redstoneBorderToQuarryBlock.get(locations);

                    if (ArrayUtils.contains(QuarryCommand.quarryBlockLocations, quarryBlockLocation)) {

                        Block quarryBlock = quarryBlockLocation.getBlock();
                        quarryBlock.setType(Material.AIR);

                        QuarryCommand.quarryBlockLocations = (Location[]) ArrayUtils.removeElement(QuarryCommand.quarryBlockLocations, quarryBlockLocation);

                        p.sendMessage(ChatColor.DARK_RED + "Quarry has been demolished.");

                        //Break all the redstone in the quarry
                        for (Location redstoneLocation : locations) {

                            Block redstone = redstoneLocation.getBlock();
                            redstone.breakNaturally();
                        }
                    }
                }
            }
        }
    }

    @EventHandler
    public void onQuarryStoneBreak(BlockBreakEvent e) {

        Player p = e.getPlayer();
        Block b = e.getBlock();
        Material m = b.getType();
        Location l = b.getLocation();

        if (m == Material.STONE) {

            if (ArrayUtils.contains(QuarryCommand.quarryBlockLocations, l)) {

                e.setCancelled(true);

                Quarry quarry = QuarryCommand.locationToQuarry.get(l);

                Block nextBlock = quarry.getNextBlock(p);

                nextBlock.breakNaturally();

            }
        }
    }
}
